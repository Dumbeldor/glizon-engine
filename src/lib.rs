mod camera;
mod state;
mod texture;

use std::path::Path;
use wgpu::util::DeviceExt;
use winit::platform::windows::IconExtWindows;
use winit::{
    event::*,
    event_loop::{ControlFlow, EventLoop},
    window::WindowBuilder,
};

use winit::window::{Icon, Window};

pub async fn run() {
    env_logger::init();
    let icon_path = format!(
        "{}/src/assets/glizon_engine.ico",
        env!("CARGO_MANIFEST_DIR")
    );
    let icon = Icon::from_path(Path::new(&icon_path), None).unwrap();
    let event_loop = EventLoop::new();
    let window = WindowBuilder::new()
        .with_title("Glizon Engine")
        .with_window_icon(Some(icon))
        .build(&event_loop)
        .unwrap();

    let mut state = state::State::new(window).await;

    event_loop.run(move |event, _, control_flow| match event {
        Event::RedrawRequested(window_id) if window_id == state.window().id() => {
            state.update();
            match state.render() {
                Ok(_) => {}
                Err(wgpu::SurfaceError::Lost) => state.resize(state.size),
                Err(wgpu::SurfaceError::OutOfMemory) => *control_flow = ControlFlow::Exit,
                Err(e) => eprintln!("{:?}", e),
            }
        }
        Event::MainEventsCleared => {
            state.window().request_redraw();
        }
        Event::WindowEvent {
            ref event,
            window_id,
        } if window_id == state.window.id() => {
            if !state.input(event) {
                match event {
                    WindowEvent::CloseRequested
                    | WindowEvent::KeyboardInput {
                        input:
                            KeyboardInput {
                                state: ElementState::Pressed,
                                virtual_keycode: Some(VirtualKeyCode::Escape),
                                ..
                            },
                        ..
                    } => *control_flow = ControlFlow::Exit,
                    WindowEvent::Resized(physical_size) => {
                        state.resize(*physical_size);
                    }
                    WindowEvent::ScaleFactorChanged { new_inner_size, .. } => {
                        state.resize(**new_inner_size);
                    }
                    WindowEvent::CursorMoved { position, .. } => {
                        state.cursor_moved(*position);
                    }
                    _ => {}
                }
            }
        }
        _ => {}
    });
}
